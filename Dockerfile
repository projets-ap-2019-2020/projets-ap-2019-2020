# build command : docker build --build-arg PROJET=repoName -t repoName:latest .

FROM adiloub27/serve-java:latest

ARG PROJET

COPY ${PROJET} /root/projet

WORKDIR /root/projet

RUN chmod 755 ./compile.sh ./run.sh

RUN ./compile.sh

WORKDIR /root/serve

CMD [ "node", "index.js" ]
